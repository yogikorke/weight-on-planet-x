import 'package:flutter/material.dart';

class home_screen extends StatefulWidget {
  @override
  _home_screenState createState() => _home_screenState();
}

class _home_screenState extends State<home_screen> {
  int radio_button_value = 0;
  TextEditingController input_weight_controller = new TextEditingController();

  int pluto_multiplier = 2;
  int mars_multiplier = 4;
  int venus_multiplier = 6;

  int calculated_weight = 0;
  int weight_on_earth = 0;

  String selected_planet = "";

  void radio_button_tapped(int value) {
    setState(() {
      radio_button_value = value;
    });

    if (input_weight_controller.text.isNotEmpty) {
      weight_on_earth = int.parse(input_weight_controller.text);

      if (value == 0) {
        selected_planet = "Your weight on planet Pluto";
        calculated_weight = weight_on_earth * pluto_multiplier;
      } else if (value == 1) {
        selected_planet = "Your weight on planet Mars";
        calculated_weight = weight_on_earth * mars_multiplier;
      } else if (value == 2) {
        selected_planet = "Your weight on planet Venus";
        calculated_weight = weight_on_earth * venus_multiplier;
      }
    } else {
      selected_planet = "NA";
      calculated_weight = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weight on Planet X"),
        backgroundColor: Colors.blue[900],
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset("images/planets.png", width: 200, height: 200),
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: input_weight_controller,
                style: TextStyle(color: Colors.blue[900], fontSize: 20),
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.accessibility,
                    size: 50,
                    color: Colors.blue[900],
                  ),
                  labelText: "Weight on Earth in Kgs",
                  hintText: "Weight on Earth in Kgs",
                ),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Radio(
                value: 0,
                groupValue: radio_button_value,
                onChanged: radio_button_tapped,
                activeColor: Colors.blue[900],
              ),
              Text(
                "Pluto",
                style: TextStyle(fontSize: 20),
              ),
              Radio(
                value: 1,
                groupValue: radio_button_value,
                onChanged: radio_button_tapped,
                activeColor: Colors.blue[900],
              ),
              Text(
                "Mars",
                style: TextStyle(fontSize: 20),
              ),
              Radio(
                value: 2,
                groupValue: radio_button_value,
                onChanged: radio_button_tapped,
                activeColor: Colors.blue[900],
              ),
              Text(
                "Venus",
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
          Center(
              child: Text(
            "$selected_planet $calculated_weight",
            style: TextStyle(fontSize: 20),
          ))
        ],
      ),
    );
  }
}
